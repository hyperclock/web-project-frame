# Web Project Frame

This is a PHP Framework, designed using MVC, OOP & PDO within the core.

## Requirements
* **Apache2** recommended
  * **nGinX** and others, not tested.
* **PHP 7+**
* **MySQL 5.3+**

## **Core Features**
* Completely [Opensource](LICENSE)
* Full ***MVC*** workflow
* Will use ***htaccess*** and ***mod_rewrite*** for nice urls
* Will have a *base controller* to load *models* & *views*
* Will have a database library class that uses **PDO**
* ***Post Sharing App*** built on the **Web Project Frame**
* Will include full ***user authentication*** & ***access control***
* Will have full ***CRUD*** functionality for posts.
* Will include *extra* ***helpers*** for little things like redirecting or flash messages
* ***Bootstrap4*** is used in the UI.

## Planned **Future Features**
* Article Pages (Blog/News)
* Single Pages (Legal Pages, ??)
* European Cookie Law Compliance
* Multilingual (i18n, interface and content)

## More Info
*Will be added as project grows*
