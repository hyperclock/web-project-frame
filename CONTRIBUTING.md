# Contributing to the ***Web Project Frame (WPF)***

As an open source project, The ***Web Project Frame (WPF)*** welcomes contributions of many forms.

## Bug reporting

We appreciate your effort to improve the ***Web Project Frame (WPF)*** by submitting a bug report. Before doing so, please check the following things:

1. Check whether the bug you face **hasn't been already reported**. Duplicate reports takes us time, that we could be used to fix other bugs or make improvements.
2. Specify the the ***Web Project Frame (WPF)***, server, PHP, MySQL and browser information that may be helpful to fix the problem, especially exact **version numbers**.
3. If you got some error, please **describe what happened** and add error message. Reports like "I get error when I clicked on some link." are useless.
4. Provide easy steps to reproduce and if possible include everything even if you think it's not needed.
5. **Security problems** should also be reported here. See [our security page](https://gitlab.com/hyperclock/web-project-frame/issues).
Thanks for your help!

Please report [bugs on GitLab][1].

[1]: (https://gitlab.com/hyperclock/web-project-frame/issues/new)

## Patches submission

Patches are welcome as [pull requests on GitLab][2].  Please include a
Signed-off-by tag in the commit message (you can do this by passing `--signoff`
parameter to Git).

When creating the commit on GitLab or using some other tool which does not have
direct support for this, it is the same as adding
`Signed-off-by: Your name <email@example.com>`
as the last line of the commit message.

Note that by submitting patches with the Signed-off-by tag, you are giving
permission to license the patch as GPLv2-or-later.  See [the DCO file][3] for
details.


[2]: https://gitlab.com/hyperclock/web-project-frame/pulls
[3]: https://gitlab.com/hyperclock/web-project-frame/blob/master/DCO

## More information

You can find more information on our [wiki](https://gitlab.com/hyperclock/web-project-frame/wiki)
